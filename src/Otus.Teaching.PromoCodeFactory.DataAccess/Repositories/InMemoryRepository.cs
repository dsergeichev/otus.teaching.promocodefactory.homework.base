﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return Data;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return Data.FirstOrDefault(x => x.Id == id);
        }

        public async Task<Guid> CreateAsync(T entity)
        {
            var id = Guid.NewGuid();
            entity.Id = id;
            Data.Add(entity);
            return id;
        }

        public async Task<int> DeleteByIdAsync(Guid id)
        {
            var deleteEntity = await GetByIdAsync(id);
            if (deleteEntity != null)
            {
                Data.Remove(deleteEntity);
                return 1;
            }
            return 0;
        }

        public async Task<int> UpdateAsync(T entity)
        {
            var updateEntity = await GetByIdAsync(entity.Id);

            if (updateEntity != null)
            {
                var index = Data.IndexOf(updateEntity);
                Data[index] = entity;
                return 1;
            }
            return 0;
        }
    }
}